// Non Mutator Methds
/*
	- Non-Mutator methods are functions that do not modify or change an array after they have been created
	- These methods do not manipulate the original array
	- These methods perform tasks such as returning (or getting) elements from an array, combining arrays, and printing the output
	- indexOf(), lastIndexOf(), slice(), toString(), concat(), join()
*/
console.log("Non-Mutator Methods");
let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];
console.log(countries);
console.log("");


// indexOf
/*
	- Returns the index number of the "first matching element" found in an array
	- If no match has been found, the result will be -1
	- The search process will be done from the first element proceeding to the last element.
	- Syntax:
		arrayName.indexOf(searchValue);
		arrayName.indexOf(searchValue, fromIndex);
*/
console.log("indexOf Method");
let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf method: " + firstIndex);

let invalidCountry = countries.indexOf("BR");
console.log("Result of indexOf method: " + invalidCountry);
console.log("");


// lastIndexOf
/*
	- Returns the index number of the "last" matching element" found in an array
	- If no match has been found, the result will be -1
	- The search process will be done from the last element proceeding to the first element.
	- Syntax:
		arrayName.lastIndexOf(searchValue);
		arrayName.lastIndexOf(searchValue, fromIndex);
*/
console.log("lastIndexOf Method");
let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf method: " + lastIndex);

// Getting the index number starting from a specified index
let lastIndexStart = countries.lastIndexOf("PH", 6);
let lastIndexStarter = countries.lastIndexOf("PH", 4);
console.log("Result of lastIndexOf method: " + lastIndexStart);
console.log("Result of lastIndexOf method: " + lastIndexStarter);
console.log("");


// slice()
/*
	- Slices elements from an array AND returns a new array
	- Syntax:
		arrayName.slice(startingIndex);
		arrayName.slice(startingIndex, endingIndex);
*/
console.log("slice Method");

// Slicing-off elements from a specified index to the last element
let sliceArrayA = countries.slice(2);
console.log("Result of slice method: ");
console.log(sliceArrayA);

// Slicing off elements from a specified index to another index
let sliceArrayB = countries.slice(2, 4);
console.log("Result of slice method: ");
console.log(sliceArrayB);

// Slicing off elements starting from the last element of an array
let sliceArrayC = countries.slice(-3);
console.log("Result of slice method: ");
console.log(sliceArrayC);
console.log("");


// toString()
/*
	- Returns an array as a string seperated by commas
	- Syntax:
		arrayName.toString()
*/
console.log("toSring Method");

let stringArray = countries.toString();
console.log("Result from toString method:");
console.log(stringArray);
console.log("");


// concat()
/*
	- Combines two arrays and returns the combined result
	- Syntax:
		arrayA.concat(arrayB);
		arrayB.conct(elementA, elementB);
*/
console.log("concat Method");
let tasksArrayA = ["drink HTML", "eat JavaScript"];
let tasksArrayB = ["inhale CSS", "breathe SASS"];
let tasksArrayC = ["get git", "be node"];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log("Result from concat method:");
console.log(tasks);

// Combining multiple arrays
console.log("Result from concat method:");
let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log(allTasks);

// Combining arrays with elements
let combinedTasks = tasksArrayA.concat("smell express", "throw react");
console.log(combinedTasks);
console.log("");

// join()
/*
	- Returns an array as a string seperated by specified seperator string
	- Syntax:
		arrayName.join("seperatorString");
*/
console.log("join Method");
let users = ["John", "Jane", "Joe", "Robert"];
console.log(users.join());
console.log(users.join(" "));
console.log(users.join(" - "));
console.log(users.join(" & "));

console.log("");



console.log("--end of non-mutator methods--");


console.log("Iteration Methods");

// Iteration Methods
/*
	- Iteration methods are loops designed to perform repetitive tasks on arrays
	- Useful for manipulating array data resulting in complex tasks.
*/


// forEach()
/*
	- Similar to the for loop that iterates on each array element
	- Variable names for arrays are usually written in the plural form of the data stored in an array
	- It is common practice to use the singular form of the array content for parameter names used in array loops
	- Array iteration methods normally work with a function supplied as an argument
	- Syntax:
		arrayName.forEach(function(individualElement) {statement})
*/

allTasks.forEach(function(task) {
	console.log(task)
});

let students = ['Jeru', 'Annejanette', 'Glyn', 'Jake', 'Gabryl', 'Daniel'];

students.forEach((student) => {
	console.log("Hi, " + student + "!");
});

console.log("");


// Using forEach with conditional statements
console.log(" Filtered forEach:");

let filteredTasks = [];

allTasks.forEach(function(task) {
	if (task.length > 10) {
		console.log(task);
		filteredTasks.push(task);
	}
});

console.log("Result of filtered tasks:");
console.log(filteredTasks);

students.forEach(function(student) {
	if (student.length <= 4) {
		console.log("Sa kanan ng room uupo si " + student);
	} else {
		console.log("San kaliwa ng room uupo si " + student);
	}
});

console.log("");

// map()
/*
	- It iterates over each element AND returns new array with different values depending on the result of the function's operation
	- This is useful for performing tasks where mutating/changing the elements are required
	- Unlike the forEach method, the map method requires the use of a "return" statement in order to create another array with the performed operation
	- Syntax:
		let/const resultArray = arrayName.map(function(individualElement));
*/
let numbers = [1, 2, 3, 4, 5];
console.log("Before the map method: " + numbers);

let numberMap = numbers.map(function(number) {
	return number * number;
});

console.log("Result of map method:");
console.log(numberMap);
console.log("");


// every()
/*
	- Checks if "all" elements in an array meet the given condition
	- This is useful for validating data stored in arrays especially when dealing with large amounts of data.
	- Returns the true value if all elements meet the condition and returns false if otherwise.
	- Syntax:
		let/const resultArray = arrayName.every(function(element) {
			return expression / condition;
		});
*/
console.log("every method");

let allValid = numbers.every(function(number) {
	return number < 3;
});
console.log("Result of every method:");
console.log(allValid);
console.log("");


// some()
/*
	- Checks if at least one element in the array meets the given condition
	- Returns the true value if at least one element meets the condition and returns false if otherwise.
	- Syntax:
		let/const resultArray = arrayName.some(function(element) {
			return expression / condition;
		});
*/
console.log("some method");

let someValid = numbers.some(function(number) {
	return number < 2;
});

console.log("Result of some method:");
console.log(someValid);

// Combining the returned result fromt the every/some method may be used in other statements to perform consecutive
if (someValid) {
	console.log("Some numbers in the array are greater than 2");
}

console.log("");


// filter()
/*
	- Returns new array that contains elements which meets the given condition
	- Returns an empty array if no elements are found
	- Useful for filtering array elements with a given condition and shorten the syntax compared to using other array iteration methods
	- Syntax:
		let/const resultArray = arrayName.filter(function(element) {
			return expresssion / condition;
		});
*/
console.log("filter method");

let filterValid = numbers.filter(function(number) {
	return number < 3;
});
console.log("Result of the filter method:");
console.log(filterValid);

let nothingFound = numbers.filter(function(number) {
	return number === 0;
});
console.log(nothingFound);

// Filtering using forEach
let filteredNumbers = [];

numbers.forEach(function(number) {
	console.log(number);

	if (number < 3) {
		filteredNumbers.push(number)
	}
});
console.log("Result of filter method");
console.log(filteredNumbers);


// includes()
/*
	- Methods can be "chained" by using them one after another
	- The result of the first method is used on the second method until all chained methods have been solved.
*/
console.log("includes method");

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let filteredProducts = products.filter(function(product) {
	return product.toLowerCase().includes("a");
});

console.log(filteredProducts);
console.log("");


// reduce()
/*
	- Evaluates statements from left to right and returns/reduces the array into a single value.
	- Syntax:
		let/const resultArr = arrayName.reduce(function(accumulator, currentValue) {
			return expression / condition;
		});
	- The "accumulator" paramater in the function stores the result for every iteration of the loop
	- The "currentValue" is the current / next element in the array that is evaluated in each iteration of the loop
*/

let iteration = 0;
console.log(numbers);

let reducedArray = numbers.reduce(function(x, y) {
	console.warn("current iteration: " + ++iteration);
	console.log("accumulator: " + x);
	console.log("currentValue: " + y);

	// The operation to reduce the array into a single value
	return x + y;
});

console.log("Result of reduce method: " + reducedArray);


// Reducing string arrays
let list = ['Hello', 'Again', 'World'];

let reduceJoin = list.reduce(function(x, y) {
	return x + ' ' + y;
});

console.log("Result of reduce method: " + reduceJoin);